package com.ibeetl.code.javadoc;

/**
 * 这是一个 `markdown-doclet` 例子.
 * <p>
 * ## 大标题
 * ### 小标题
 * <p>
 * <p>
 * <p>
 * 代码块，
 * <p>
 * ```java
 * int foo = 42;
 * System.out.println(foo);
 * .@Autowird UserService service ;
 * ```
 * <p>
 * > 如果代码块里出现@ 需要在前面加上`.`
 * <p>
 * 列表:
 * <p>
 * - 第一项
 * - 第二项
 * <p>
 * Foo | Bar
 * ----|----
 * A   | B
 * C   | D
 *
 * @author ll
 * @author 公众号 java系统优化
 */
public class MarkdownDoc {
}
