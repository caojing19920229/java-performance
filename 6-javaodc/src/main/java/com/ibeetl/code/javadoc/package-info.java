/**
 * 商品和价格定义，以及通过rpc接口获取到商品和价格信息.
 *
 * @author 公众号 java系统优化
 * @see com.ibeetl.code.javadoc.Product
 * @see com.ibeetl.code.javadoc.ProductService
 * @see com.ibeetl.code.javadoc.RpcPriceService
 */
package com.ibeetl.code.javadoc;